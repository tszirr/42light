#include <stddef.h>

// type names
#ifndef flgt_char
	#define flgt_char char
#endif

// #define FLGT_CPP

#ifdef FLGT_NAMESPACE
	#define FLGT_NAMESPACE_BEGIN namespace FLGT_NAMESPACE {
	#define FLGT_NAMESPACE_END }

	#ifndef FLGT_STRUCT
		#define FLGT_STRUCT(x) x
	#endif
	#ifndef FLGT_FUN
		#define FLGT_FUN(x) x
	#endif
#else
	#ifndef FLGT_STRUCT
		#define FLGT_STRUCT(x) flgt_##x
	#endif
	#ifndef FLGT_FUN
		#define FLGT_FUN(x) flgt_##x
	#endif

	#define FLGT_NAMESPACE_BEGIN
	#define FLGT_NAMESPACE_END
#endif

#ifndef flgt_assert
	#ifdef LGT_ASSERT
		#define flgt_assert(x) LGT_ASSERT(x)
	#else
		#define flgt_assert(x) assert(x)
	#endif 
#endif
#ifndef flgt_warn
	#ifdef LGT_WARN
		#define flgt_warn(x, ...) LGT_WARN(x, ##__VA_ARGS__)
	#else
		#define flgt_warn(x, ...) printf(x, ##__VA_ARGS__)
	#endif 
#endif

#define flgt_keyval_format(x, y, z) ( sizeof(flgt_char) == sizeof(wchar_t) ? (x "ls" y "ls" z) : (x "s" y "s" z) )

FLGT_NAMESPACE_BEGIN

// characters
#ifndef flgt_esc
	#define flgt_esc ((flgt_char)'\\')
#endif
#ifndef flgt_assign
	#define flgt_assign ((flgt_char)':')
#endif
#if !defined(flgt_group_begin) && !defined(flgt_group_end)
	#define flgt_group_begin ((flgt_char)'{')
	#define flgt_group_end   ((flgt_char)'}')
#endif
#ifndef flgt_quote
	#define flgt_quote ((flgt_char)'"')
#endif
#ifndef flgt_endl
	#define flgt_endl ((flgt_char)'\n')
#endif
#ifndef flgt_delimiter
	#define flgt_delimiter ((flgt_char)',')
#endif
// todo: comments syntax?
#ifndef flgt_is_space
	inline int FLGT_FUN(is_space)(flgt_char c) {
		return c == ((flgt_char)' ') || c == ((flgt_char)'\t') || c == ((flgt_char)'\r') || c == ((flgt_char)'\n');
	}
	#define flgt_is_space(x) FLGT_FUN(is_space)(x)
#endif

typedef struct FLGT_STRUCT(text_data_state)
	FLGT_STRUCT(text_data_state);
typedef struct FLGT_STRUCT(text_data_handler)
	FLGT_STRUCT(text_data_handler);

struct FLGT_STRUCT(text_data_handler) {
	void* user;
	// called for key=value
	int (*attribute)(FLGT_STRUCT(text_data_handler) const* handler
		, flgt_char const* key, flgt_char const* keyEnd
		, flgt_char const* value, flgt_char const* valueEnd);
	// called for [key=value, ...]
	int (*group)(FLGT_STRUCT(text_data_handler) const* handler
		, FLGT_STRUCT(text_data_state)* state
		, flgt_char const* firstKey, flgt_char const* keyEnd
		, flgt_char const* firstValue, flgt_char const* valueEnd);
	// called for outerKey:[key=value, ...], if given, otherwise
	// if null calls [key=value], then outerKey=value
	int (*aggregate_attribute)(FLGT_STRUCT(text_data_handler) const* handler
		, flgt_char const* outerKey, flgt_char const* outerKeyEnd
		, FLGT_STRUCT(text_data_state)* state);
};

struct FLGT_STRUCT(text_data_state) {
	flgt_char const* cursor, *end;
	FLGT_STRUCT(text_data_handler) const* parent_handler;
};

inline flgt_char const* FLGT_FUN(next_space)(flgt_char const* cur, flgt_char const* end) {	
	while (cur < end && !flgt_is_space(*cur) && *cur != flgt_delimiter)
		++cur;
	return cur;
}
inline flgt_char const* FLGT_FUN(skip_space)(flgt_char const* cur, flgt_char const* end) {	
	while (cur < end && (flgt_is_space(*cur) || *cur == flgt_delimiter))
		++cur;
	return cur;
}

inline flgt_char const* FLGT_FUN(trim_left)(flgt_char const* cur, flgt_char const* end) {	
	while (cur < end && flgt_is_space(*cur) && *cur != flgt_endl)
		++cur;
	return cur;
}
inline flgt_char const* FLGT_FUN(trim_right)(flgt_char const* cur, flgt_char const* end) {	
	while (cur < end && flgt_is_space(end[-1]))
		--end;
	return end;
}
inline flgt_char const* FLGT_FUN(find_special_char_before)(flgt_char c, flgt_char const* cur, flgt_char const* end, flgt_char beforeC) {
	while (cur < end && *cur != c && *cur != beforeC)
		++cur;
	return cur;
}

// note: quotation logic is the only thing that requires escaping
inline flgt_char const* FLGT_FUN(count_opening_quotes)(flgt_char const* cur, flgt_char const* end, int* quoteCountOut) {	
	int qc = 0;
	// number of leading quotes defines number of closing quotes
	while (cur < end && *cur == flgt_quote) {
		++cur;
		++qc;
	}
	// first backslash inside quotes is always swallowed
	if (qc && *cur == flgt_esc)
		++cur;
	*quoteCountOut = qc;
	return cur;
}
inline flgt_char const* FLGT_FUN(find_end_quotes)(flgt_char const* cur, flgt_char const** quotationEndOut, flgt_char const* end, int openQuoteCount) {
	if (openQuoteCount == 0) {
		*quotationEndOut = cur;
		return cur;
	}
	int qc = 0;
	flgt_char const* speculativeEnd = cur;
	// given number of quotes defines number of closing quotes
	while (cur < end) {
		if (*cur == flgt_quote) {
			if (qc++ == 0)
				speculativeEnd = cur;
			if (qc == openQuoteCount) {
				*quotationEndOut = cur + 1;
				return speculativeEnd;
			}
		}
		else
			qc = 0;
		++cur;
	}
	// todo: add location info?
	flgt_warn("no closing quotations found, string encompasses the rest of the input");
	*quotationEndOut = cur;
	return cur;
}

struct FLGT_STRUCT(text_data_group_value) {
	flgt_char const* start, *end;
};
inline int FLGT_FUN(parse_group)(FLGT_STRUCT(text_data_handler) const* handler, FLGT_STRUCT(text_data_state)* state, struct FLGT_STRUCT(text_data_group_value)* init_group);
inline FLGT_STRUCT(text_data_handler) FLGT_FUN(skip_text_handler)(flgt_char const* fromOrEcho);

// note: expects state to be pre-whitespace-trimmed
inline int FLGT_FUN(parse_attribute)(FLGT_STRUCT(text_data_handler) const* handler, FLGT_STRUCT(text_data_state)* state, struct FLGT_STRUCT(text_data_group_value)* obtain_new_group_value) {
	flgt_char const* attributeCursor = state->cursor;
	flgt_assert(attributeCursor != state->end && !flgt_is_space(*attributeCursor));

	// key or value may start with quotes
	int kqc;
	flgt_char const* keyStart = FLGT_FUN(count_opening_quotes)(attributeCursor, state->end, &kqc);
	flgt_char const* keyEnd = FLGT_FUN(find_end_quotes)(keyStart, &attributeCursor, state->end, kqc);

	// find next token on the same line
	if (kqc > 0)
		attributeCursor = FLGT_FUN(trim_left)(attributeCursor, state->end);
	// if key was unquoted, it may end with an assignment on the same line
	else
		attributeCursor = FLGT_FUN(find_special_char_before)(flgt_assign, keyStart, state->end, flgt_endl);

	flgt_char const* valueStart = keyStart, *valueEnd = keyEnd;
	int vqc = kqc;
	// optional assignment indicates key
	if (attributeCursor < state->end && *attributeCursor == flgt_assign) {
		// unquoted keys are trimmed from the left of the assignment
		if (!kqc)
			keyEnd = FLGT_FUN(trim_right)(keyStart, attributeCursor);

		// value is trimmed from the right or starts with quotes, both on the same line
		attributeCursor = FLGT_FUN(trim_left)(attributeCursor + 1, state->end);
		valueStart = FLGT_FUN(count_opening_quotes)(attributeCursor, state->end, &vqc);
		valueEnd = FLGT_FUN(find_end_quotes)(valueStart, &attributeCursor, state->end, vqc);
	}
	// no key, value only
	else
		keyEnd = keyStart;

	// if value was unquoted, it ends with the next space
	if (!vqc)
		valueEnd = FLGT_FUN(next_space)(valueStart, state->end);

	// nested independent group
	if (obtain_new_group_value) {
		obtain_new_group_value->start = valueStart;
		obtain_new_group_value->end = valueEnd;
		return handler->group(handler, state, keyStart, keyEnd, valueStart, valueEnd);
	}
	else {
		// nested aggregate values or independent groups start after assignment
		if (!vqc && valueStart < valueEnd && *valueStart == flgt_group_begin) {
			// check for special aggregate attribute handling
			if (handler->aggregate_attribute) {
				state->cursor = valueStart + 1;
				int r = handler->aggregate_attribute(handler, keyStart, keyEnd, state);
				// if not rejected, assert that group was fully parsed
				if (state->cursor > valueStart) {
					// handler refused, skip aggregate with warnings
					if (state->cursor == valueStart + 1) {
						FLGT_STRUCT(text_data_handler) skip = FLGT_FUN(skip_text_handler)(0);
						r |= FLGT_FUN(parse_group)(&skip, state, 0);
					}
					flgt_assert(state->cursor[-1] == flgt_group_end);
					return r;
				}
			}
			// no special handling, or aggregate attribute refused
			state->cursor = valueStart + 1;
			{
				struct FLGT_STRUCT(text_data_group_value) aggregate_value = { 0 };
				// default handling as independent group _inside parent_
				int r = FLGT_FUN(parse_group)(state->parent_handler ? state->parent_handler : handler, state, &aggregate_value);
				// if successful, pass result as plain attribute
				if (r == 0)
					r |= handler->attribute(handler, keyStart, keyEnd, aggregate_value.start, aggregate_value.end);
				return r;
			}
		}
		else {
			int r = handler->attribute(handler, keyStart, keyEnd, valueStart, valueEnd);
			state->cursor = valueEnd;
			return r;
		}
	}
}

inline int FLGT_FUN(parse_group)(FLGT_STRUCT(text_data_handler) const* handler, FLGT_STRUCT(text_data_state)* state, struct FLGT_STRUCT(text_data_group_value)* init_group
#ifdef __cplusplus
	= NULL
#endif
	) {
	FLGT_STRUCT(text_data_handler) const* parent_handler = state->parent_handler;
	if (init_group)
		state->parent_handler = handler;
	int r = 0;
	// key or group starts with first non-whitespace
	while ((state->cursor = FLGT_FUN(skip_space)(state->cursor, state->end)) < state->end) {
		flgt_char const* lastCursor = state->cursor;
		// enclosing group ends
		if (*state->cursor == flgt_group_end) {
			++state->cursor;
			break;
		}
		// nested independent groups start at the beginning
		else if (*state->cursor == flgt_group_begin) {
			if (init_group) {
				// group starts with another group, initialize without additional info
				r |= handler->group(handler, state, state->cursor, state->cursor, state->cursor, state->cursor);
			}
			else {
				// descend into nested group
				++state->cursor;
				struct FLGT_STRUCT(text_data_group_value) init_nested_group = { 0 };
				r |= FLGT_FUN(parse_group)(handler, state, &init_nested_group);
			}
		}
		else {
			// either start current group or handle additional attributes
			r |= FLGT_FUN(parse_attribute)(handler, state, init_group);
		}
		
		if (init_group) {
			// group was refused, skip w/ warning
			if (state->cursor <= lastCursor) {
				FLGT_STRUCT(text_data_handler) skip = FLGT_FUN(skip_text_handler)(0);
				r |= FLGT_FUN(parse_group)(&skip, state, 0);
			}
			// assert that group was fully parsed
			flgt_assert(state->cursor[-1] == flgt_group_end);
			break;
		}
	}
	if (init_group)
		state->parent_handler = parent_handler;
	return r;
}

inline int FLGT_FUN(parse_text_groups)(FLGT_STRUCT(text_data_handler) const* handler, flgt_char const* start, flgt_char const* end, flgt_char const** pend
#ifdef __cplusplus
	= 0
#endif
	, struct FLGT_STRUCT(text_data_group_value)* launch_root
#ifdef __cplusplus
	= 0
#endif
	) {
	FLGT_STRUCT(text_data_state) state = { start, end };
	int r = FLGT_FUN(parse_group)(handler, &state, launch_root);
	if (pend)
		*pend = state.cursor;
	else if (state.cursor != state.end) {
		char const* following = start + 8 < state.cursor ? state.cursor - 8 : start;
		flgt_warn(flgt_keyval_format("parsing ended prematurely, ignoring '%.*", "' following '%.*", "'\n")
			, (int)(state.end - state.cursor), state.cursor
			, (int)(state.cursor - following), following);
	}
	return r;
}

// called for key=value
inline int FLGT_FUN(_skip_attribute)(FLGT_STRUCT(text_data_handler) const* handler
	, flgt_char const* key, flgt_char const* keyEnd
	, flgt_char const* value, flgt_char const* valueEnd) {
	if (!handler->user)
		flgt_warn(flgt_keyval_format("ignored attribute '%.*", "'='%.*", "'\n"), (int)(keyEnd - key), key, (int)(valueEnd - value), value);
	return 0;
}
// called for [key=value, ...]
inline int FLGT_FUN(_skip_group)(FLGT_STRUCT(text_data_handler) const* handler
	, FLGT_STRUCT(text_data_state)* state
	, flgt_char const* firstKey, flgt_char const* keyEnd
	, flgt_char const* firstValue, flgt_char const* valueEnd) {
	if (!handler->user)
		flgt_warn(flgt_keyval_format("ignored group '%.*", "'='%.*", "'\n"), (int)(keyEnd - firstKey), firstKey, (int)(valueEnd - firstValue), firstValue);
	return FLGT_FUN(parse_group)(handler, state, 0);
}
// called for key=[ ...]
inline int FLGT_FUN(_skip_aggregate_attribute)(FLGT_STRUCT(text_data_handler) const* handler
	, flgt_char const* key, flgt_char const* keyEnd
	, FLGT_STRUCT(text_data_state)* state) {
	if (!handler->user)
		flgt_warn(flgt_keyval_format("ignored attribute '%.*", "'=[%.*", "...\n"), (int)(keyEnd - key), key
			, (int)(state->end - state->cursor > 8 ? 8 : state->end - state->cursor), state->cursor);
	return FLGT_FUN(parse_group)(handler, state, 0);
}

// constructs a handler that skips all attributes and nested groups. If passed 0, echoes a warning for every skipped entity.
inline FLGT_STRUCT(text_data_handler) FLGT_FUN(skip_text_handler)(flgt_char const* fromOrEcho
#ifdef __cplusplus
	= NULL
#endif
	) {
	FLGT_STRUCT(text_data_handler) h;
	h.user = (void*) fromOrEcho;
	h.attribute = FLGT_FUN(_skip_attribute);
	h.group = FLGT_FUN(_skip_group);
	h.aggregate_attribute = FLGT_FUN(_skip_aggregate_attribute);
	return h;
}

// retrieves the range of the current group
inline struct FLGT_STRUCT(text_data_group_value) extract_group_range(FLGT_STRUCT(text_data_state)* state) {
	struct FLGT_STRUCT(text_data_group_value) r;
	r.start = state->cursor;
	FLGT_STRUCT(text_data_handler) skip = FLGT_FUN(skip_text_handler)(r.start);
	int zero = FLGT_FUN(parse_group)(&skip, state, 0);
	r.end = state->cursor - 1;
	if (r.end < r.start) r.end = r.start;
	return r;
}

#ifdef FLGT_CPP

template <class Derived>
struct FLGT_STRUCT(text_data_handler_base) : FLGT_STRUCT(text_data_handler) {
	// called for key=value
	static int fwd_attribute(FLGT_STRUCT(text_data_handler) const* handler
		, flgt_char const* key, flgt_char const* keyEnd
		, flgt_char const* value, flgt_char const* valueEnd) {
		return ((Derived*)handler->user)->attribute(key, keyEnd, value, valueEnd);
	}
	// called for [key=value, ...]
	static int fwd_group(FLGT_STRUCT(text_data_handler) const* handler
		, FLGT_STRUCT(text_data_state)* state
		, flgt_char const* firstKey, flgt_char const* keyEnd
		, flgt_char const* firstValue, flgt_char const* valueEnd) {
		return ((Derived*)handler->user)->group(state, firstKey, keyEnd, firstValue, valueEnd);
	}
	// called for outerKey:[key=value, ...], if given, otherwise
	// if null calls [key=value], then outerKey=value
	static int fwd_aggregate_attribute(FLGT_STRUCT(text_data_handler) const* handler
		, flgt_char const* outerKey, flgt_char const* outerKeyEnd
		, FLGT_STRUCT(text_data_state)* state) {
		return ((Derived*)handler->user)->aggregate_attribute(outerKey, outerKeyEnd, state);
	}

	// default ignores nested groups with a warning
	int group(FLGT_STRUCT(text_data_state)* state
		, flgt_char const* firstKey, flgt_char const* keyEnd
		, flgt_char const* firstValue, flgt_char const* valueEnd) {
		FLGT_STRUCT(text_data_handler) skip = FLGT_FUN(skip_text_handler)();
		return FLGT_FUN(_skip_group)(&skip, state, firstKey, keyEnd, firstValue, valueEnd);
	}
	// dummy default
	int aggregate_attribute(char const* outerKey, char const* outerKeyEnd
		, FLGT_STRUCT(text_data_state)* state) {
		return 0;
	}

	text_data_handler_base() {
		this->user = (Derived*) this;
		this->attribute = &fwd_attribute;
		this->FLGT_STRUCT(text_data_handler)::group = &fwd_group;
		this->FLGT_STRUCT(text_data_handler)::aggregate_attribute =
			(&Derived::aggregate_attribute != &text_data_handler_base::aggregate_attribute)
			? &fwd_aggregate_attribute
			: NULL;
	}
};

#endif

FLGT_NAMESPACE_END

#undef FLGT_NAMESPACE_BEGIN
#undef FLGT_NAMESPACE_END
#undef flgt_esc
#undef flgt_assign
#undef flgt_group_begin
#undef flgt_group_end
#undef flgt_quote
#undef flgt_endl
#undef flgt_is_space
#undef FLGT_FUN
#undef FLGT_STRUCT
#undef flgt_char
#undef flgt_assert
#undef flgt_warn
#undef flgt_keyval_format
