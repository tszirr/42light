#include "window.h"
#include "keycodes.h"
#include <Windows.h>
#include "win32_keycodes.h"
#include <stdlib.h>
#include <string.h>

#define WLGT_CLASSNAME "wndlight-win32"
#define tmalloc(t) ((t*) malloc(sizeof(t)))

#define WM_REMOTE_REPAINT WM_USER + WM_PAINT
#define WM_JOIN_DESTROY WM_USER + WM_DESTROY

DWORD wlgt_init_time = 0;

struct ThreadData {
	void(*run)(void*);
	void* user;
	bool(*init)(void*);
	bool initialized;
	HANDLE hInitialized;
};
static DWORD WINAPI threadProc(_In_ LPVOID lpParameter) {
	ThreadData* data = (ThreadData*)lpParameter;
	void(*run)(void*) = data->run;
	void* user = data->user;

	if (data->init) {
		bool initialized = false;
		__try { initialized = data->init(data->user); }
		__finally {
			data->initialized = initialized;
			SetEvent(data->hInitialized);
		}
		if (!initialized)
			return -1;
	}
	else
		free(data);
	
	run(user);
	return 0;
}
bool create_thread(void(*run)(void*), void* user, bool(*init)(void*)) {
	ThreadData data = { run, user, init, false };
	if (init)
		data.hInitialized = CreateEventA(NULL, FALSE, FALSE, NULL);
	
	bool started;
	{
		ThreadData* sdata = &data;
		if (!init) {
			sdata = tmalloc(ThreadData);
			memcpy(sdata, &data, sizeof(data));
		}
		HANDLE hThread = CreateThread(
			NULL, // default security attributes
			0, // use default stack size  
			threadProc, // thread function name
			sdata, // argument to thread function 
			0, // use default creation flags 
			NULL);
		started = FALSE != CloseHandle(hThread);
		if (!started && sdata != &data)
			free(sdata);
	}
	
	if (init) {
		if (started) {
			WaitForSingleObject(data.hInitialized, INFINITE);
			started = data.initialized;
		}
		CloseHandle(data.hInitialized);
		data.hInitialized = NULL;
	}
	return started;
}

struct WindowBitmap {
	unsigned char* bitmap;
	HBITMAP handle, destroyHandle;
	HDC hCompatible;
};
static int const F_LAYERED = 0x1;
static int const F_BUFFERED = 0x2;
static int const F_APPLICATION = 0x4;
struct WindowData {
	int cx, cy;
	int width;
	int height;
	void* user;
	int flags;
	wlgt_callbacks callbacks;
	WindowBitmap bmp;
};
static WindowData* window_data(HWND wnd) {
	return (WindowData*) GetWindowLongPtrA(wnd, GWLP_USERDATA);
}
void* window_userdata(wlgt* wnd) {
	return window_data((HWND) wnd)->user;
}

static void delete_offscreen_surface(WindowBitmap* bmp) {
	if (bmp->destroyHandle) {
		SelectObject(bmp->hCompatible, bmp->destroyHandle);
		DeleteObject(bmp->handle);
		DeleteDC(bmp->hCompatible);
	}
	memset(bmp, 0, sizeof(WindowBitmap));
}
static void delete_offscreen_surface(HWND hWnd, WindowData* data) {
	if (data->bmp.bitmap && data->callbacks.surface_changed)
		data->callbacks.surface_changed((wlgt*) hWnd, data->user, data->width, data->height, nullptr);
	delete_offscreen_surface(&data->bmp);
}

static void create_offscreen_surface(HWND hWnd, WindowData* data, int width, int height) {
	RECT rect = { 0, 0, width, height };
	if (!width || !height)
		GetClientRect(hWnd, &rect);
	data->width = rect.right - rect.left;
	data->height = rect.bottom - rect.top;
	
	delete_offscreen_surface(hWnd, data);
	
	HDC hDC = GetDC(data->flags & F_LAYERED ? NULL : hWnd);
	HDC hCompatible = CreateCompatibleDC(hDC);
	ReleaseDC(data->flags & F_LAYERED ? NULL : hWnd, hDC);

	BITMAPINFO bmi;
	memset(&bmi, 0, sizeof(BITMAPINFO));
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = rect.right - rect.left;
	bmi.bmiHeader.biHeight = rect.top - rect.bottom; // top-down
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	void* bits = NULL;
	HBITMAP hBmp = CreateDIBSection(hCompatible, &bmi, DIB_RGB_COLORS, &bits, NULL, 0);

	if (bits) {
		data->bmp.hCompatible = hCompatible;
		data->bmp.destroyHandle = (HBITMAP)SelectObject(hCompatible, hBmp);;
		data->bmp.handle = hBmp;
		data->bmp.bitmap = (unsigned char*) bits;
	}
	else {
		DeleteObject(hBmp);
		DeleteDC(hCompatible);
	}

	if (data->callbacks.surface_changed)
		data->callbacks.surface_changed((wlgt*)hWnd, data->user, data->width, data->height, (unsigned char*) bits);
}

void update_window(wlgt* wnd) {
	::InvalidateRect((HWND) wnd, NULL, FALSE);
}

void async_update_window(wlgt* wnd) {
	::PostMessageA((HWND) wnd, WM_REMOTE_REPAINT, 0, 0);
}

void update_transparent_window(wlgt* wnd) {
	WindowData* data = window_data((HWND) wnd);
	if (data->flags & F_LAYERED && data->bmp.hCompatible) {
		BLENDFUNCTION blend = { 0 };
		blend.BlendOp = AC_SRC_OVER;
		blend.SourceConstantAlpha = 255;
		blend.AlphaFormat = AC_SRC_ALPHA;
		POINT pt = { 0, 0 };
		SIZE size = { data->width, data->height };
		RECT r;
		GetWindowRect((HWND)wnd, &r);

		HDC hDC = GetDC(NULL);
		UpdateLayeredWindow((HWND) wnd, hDC, (POINT*) &r, &size, data->bmp.hCompatible, &pt, 0, &blend, ULW_ALPHA);
		ReleaseDC(NULL, hDC);
	}
}

static LRESULT wndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {
	switch (Msg) {
	case WM_CREATE: {
		WindowData* data = (WindowData*) ((CREATESTRUCTA*) lParam)->lpCreateParams;
		SetWindowLongPtrA(hWnd, GWLP_USERDATA, (LONG_PTR) data);
		if (data->callbacks.created)
			data->callbacks.created((wlgt*) hWnd, data->user);
		data->cx = data->cy = -1;
		break;
	}
	case WM_DESTROY: {
		WindowData* data = window_data(hWnd);
		bool quit = (data->flags & F_APPLICATION) != 0;
		if (data->callbacks.destroyed)
			quit = data->callbacks.destroyed((wlgt*)hWnd, data->user);
		SetWindowLongPtrA(hWnd, GWLP_USERDATA, NULL);
		delete_offscreen_surface(hWnd, data);
		free(data);
		if (quit)
			PostQuitMessage(0);
		break;
	}
	
	case WM_DISPLAYCHANGE:
		if (WindowData* data = window_data(hWnd)) {
			if (data->flags & F_BUFFERED || data->bmp.bitmap)
				create_offscreen_surface(hWnd, data, 0, 0);
		}
		break;
	
	case WM_SHOWWINDOW:
	case WM_WINDOWPOSCHANGED:
		if (WindowData* data = window_data(hWnd)) {
			if (IsWindowVisible(hWnd) && !IsIconic(hWnd)) {
				RECT clientRect;
				GetClientRect(hWnd, &clientRect);

				SIZE size = { clientRect.right - clientRect.left, clientRect.bottom - clientRect.top };

				bool sizeChanged = data->width != size.cx || data->height != size.cy;
				if (data->flags & F_BUFFERED || data->bmp.bitmap) {
					if (!data->bmp.bitmap || sizeChanged)
						create_offscreen_surface(hWnd, data, size.cx, size.cy);
				}
				if (sizeChanged) {
					data->width = size.cx;
					data->height = size.cy;
					if (data->callbacks.size_changed)
						data->callbacks.size_changed((wlgt*)hWnd, data->user, size.cx, size.cy);
				}

				if (data->callbacks.pos_changed) {
					POINT pos = { clientRect.left, clientRect.top };
					ClientToScreen(hWnd, &pos);
					data->callbacks.pos_changed((wlgt*)hWnd, data->user, pos.x, pos.y, size.cx, size.cy);
				}

				if (data->flags & F_LAYERED)
					InvalidateRect(hWnd, NULL, FALSE);
			}
		}
		break;

	case WM_REMOTE_REPAINT:
		InvalidateRect(hWnd, NULL, FALSE);
		return 0;
		break;

	case WM_PAINT: {
		if (WindowData* data = window_data(hWnd)) {
			if (data->flags & F_LAYERED)
				update_transparent_window((wlgt*)hWnd);
			else if (data->bmp.hCompatible) {
				PAINTSTRUCT paint;
				if (HDC hDC = BeginPaint(hWnd, &paint)) {
					BitBlt(hDC, 0, 0, data->width, data->height, data->bmp.hCompatible, 0, 0, SRCCOPY);
					EndPaint(hWnd, &paint);
				}
			}
			return 0;
		}
		break;
	}

	case WM_KEYDOWN: case WM_SYSKEYDOWN:
	case WM_KEYUP: case WM_SYSKEYUP:
		if (WindowData* data = window_data(hWnd)) {
			auto callback = (Msg == WM_KEYDOWN || Msg == WM_SYSKEYDOWN) ? data->callbacks.key_down : data->callbacks.key_up;
			if (callback) {
				DWORD time = GetMessageTime();
				int key = wlgt_translate_key(wParam, lParam, time);
				callback((wlgt*) hWnd, data->user, key, time - wlgt_init_time);
			}
		}
		break;

	case WM_LBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
		if (WindowData* data = window_data(hWnd)) {
			auto callback = int(Msg - WM_LBUTTONDOWN) % 3 ? data->callbacks.button_up : data->callbacks.button_down;
			if (callback) {
				int button = 1 << (int(Msg - WM_LBUTTONDOWN) / 3);
				DWORD time = GetMessageTime();
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				float fx = x / float(data->width);
				float fy = y / float(data->height);
				callback((wlgt*) hWnd, data->user, button, x, y, fx, fy, time - wlgt_init_time);
			}
		}
		break;

	case WM_MOUSELEAVE:
		if (WindowData* data = window_data(hWnd)) {
			data->cx = data->cy = -1;
		}
		break;

	case WM_MOUSEMOVE:
		if (WindowData* data = window_data(hWnd)) {
			if (data->callbacks.cursor_pos || data->callbacks.cursor_delta) {
				int buttons = wParam & (MK_LBUTTON | MK_RBUTTON);
				buttons |= (wParam & MK_MBUTTON) >> 2;
				DWORD time = GetMessageTime();
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				if (data->callbacks.cursor_pos) {
					float fx = x / float(data->width);
					float fy = y / float(data->height);
					data->callbacks.cursor_pos((wlgt*) hWnd, data->user, x, y, fx, fy, buttons, time - wlgt_init_time);
				}
				if (data->callbacks.cursor_delta) {
					if (data->cx >= 0 && data->cy >= 0) {
						int dx = x - data->cx;
						int dy = y - data->cy;
						float fx = dx / float(data->width);
						float fy = dy / float(data->height);
						data->callbacks.cursor_delta((wlgt*) hWnd, data->user, dx, dy, fx, fy, buttons, time - wlgt_init_time);
					}
					data->cx = x;
					data->cy = y;
				}
			}
		}
		break;

	case WM_CLOSE:
		if (WindowData* data = window_data(hWnd)) {
			if (data->callbacks.closed)
				if (!data->callbacks.closed((wlgt*) hWnd, data->user))
					return -1; // abort close
		}
		break;

	case WM_JOIN_DESTROY:
		DestroyWindow(hWnd);
		return (LRESULT) hWnd;
		break;
	}

	return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

bool wlgt_pump(bool wait_for_event) {
	if (wait_for_event)
		WaitMessage();

	MSG msg;
	while (PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE)) {
		if (msg.message == WM_QUIT)
			return false; // todo: call close handlers?
		TranslateMessage(&msg);
		DispatchMessageA(&msg);
	}
	return true;
}

void wlgt_loop()
{
	while (wlgt_pump(true));
}

long long wlgt_time() {
	return ::GetTickCount();
}

static bool regiser_wnd_class() {
	WNDCLASSEXA wc;

	ZeroMemory(&wc, sizeof(wc));
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)wndProc;
	wc.hInstance = GetModuleHandleW(NULL);
	wc.hCursor = LoadCursorA(NULL, IDC_ARROW);
	wc.lpszClassName = WLGT_CLASSNAME;
	wc.hIcon = (HICON)LoadImageA(NULL,
		IDI_APPLICATION, IMAGE_ICON,
		0, 0, LR_DEFAULTSIZE | LR_SHARED);

	if (!RegisterClassExA(&wc))
		return false; // todo: error

	return true;
}

static bool unregiser_wnd_class() {
	return FALSE != UnregisterClassA(WLGT_CLASSNAME, GetModuleHandleA(NULL));
}

wlgt* create_window(char const* name, wlgt_rect const* rect, wlgt_flags const* flags, wlgt_callbacks const* callbacks, void* user, wlgt* owner) {
	DWORD exStyle = 0;
	DWORD style = WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (flags->appwindow)
		exStyle |= WS_EX_APPWINDOW;

	if (flags->fullscreen)
		style |= WS_POPUP;
	else {
		if (flags->appwindow || flags->decorated)
			style |= WS_SYSMENU | WS_MINIMIZEBOX;
		if (flags->decorated) {
			style |= WS_CAPTION;
			if (flags->resizeable)
				style |= WS_MAXIMIZEBOX | WS_SIZEBOX;
		}
		else {
			style |= WS_POPUP;
			if (!flags->appwindow)
				exStyle |= WS_EX_TOOLWINDOW;
		}
	}

	if (flags->transparent) {
		exStyle |= WS_EX_LAYERED;
	}

	WindowData* data = tmalloc(WindowData);
	memset(data, 0, sizeof(WindowData));
	memcpy(&data->callbacks, callbacks, sizeof(wlgt_callbacks));
	if (flags->transparent)
		data->flags |= F_LAYERED;
	if (!flags->bufferless)
		data->flags |= F_BUFFERED;
	if (flags->appwindow)
		data->flags |= F_APPLICATION;
	data->user = user;

	RECT windowRect = { rect->x, rect->y };
	if (windowRect.left == -1 && windowRect.top == -1) {
		windowRect.left = (GetSystemMetrics(SM_CXSCREEN) - rect->width) / 2;
		windowRect.top = (GetSystemMetrics(SM_CYSCREEN) - rect->height) / 2;
	}
	windowRect.right = windowRect.left + rect->width;
	windowRect.bottom = windowRect.top + rect->height;
	AdjustWindowRect(&windowRect, style, FALSE);

	HWND handle = CreateWindowExA(
		exStyle
		, WLGT_CLASSNAME
		, name
		, style
		, windowRect.left, windowRect.top, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top
		, (HWND) owner, NULL
		, GetModuleHandleA(NULL)
		, data
	);

	if (!handle) {
		free(data);
		// todo: error handling!
		return NULL;
	}

	return (wlgt*) handle;
}

void show_window(wlgt* handle, bool show) {
	ShowWindow((HWND)handle, show ? SW_SHOW : SW_HIDE);
}

void update_window_pos(wlgt* wnd, wlgt_rect const* rect) {
	RECT windowRect = { rect->x, rect->y };
	windowRect.right = windowRect.left + rect->width;
	windowRect.bottom = windowRect.top + rect->height;
	AdjustWindowRect(&windowRect, GetWindowLongA((HWND) wnd, GWL_STYLE), FALSE);

	SetWindowPos((HWND)wnd, NULL
		, windowRect.left, windowRect.top, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top
		, SWP_ASYNCWINDOWPOS | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
}

bool close_window(wlgt* wnd) {
	return 0 == SendMessageA((HWND)wnd, WM_CLOSE, 0, 0);
}
void force_destroy_window(wlgt* wnd) {
	DestroyWindow((HWND) wnd);
}
static HANDLE windowThread(HWND hWnd) {
	return OpenThread(SYNCHRONIZE, 0, GetWindowThreadProcessId(hWnd, NULL));
}
bool join_close_window(wlgt* wnd) {
	HANDLE hThread = windowThread((HWND) wnd);
	if (0 == SendMessageA((HWND)wnd, WM_CLOSE, 0, 0)) {
		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
		return true;
	}
	return false;
}
void join_force_destroy_window(wlgt* wnd) {
	HANDLE hThread = windowThread((HWND) wnd);
	if ((HWND)wnd == (HWND)SendMessageA((HWND)wnd, WM_JOIN_DESTROY, 0, 0)) {
		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
	}
}

bool wlgt_init() {
	wlgt_init_time = ::GetTickCount();
	wlgt_init_keys();
	return regiser_wnd_class();
}

bool wlgt_uninit() {
	return unregiser_wnd_class();
}
