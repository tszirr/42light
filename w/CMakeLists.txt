cmake_minimum_required(VERSION 3.0)
project(windlight)

set(WINDLIGHT_SRC
 window.h
 keycodes.h
)

if (WIN32)
  list(APPEND WINDLIGHT_SRC
    win32_window.cpp
    win32_keycodes.h)

  set(WINDLIGHT_DEPS user32.lib kernel32.lib)
else()
  list(APPEND WINDLIGHT_SRC
    x11_window.cpp
    x11_keycodes.h)

  set(WINDLIGHT_DEPS X11 pthread rt)
endif()

add_library(${PROJECT_NAME} ${WINDLIGHT_SRC})
target_include_directories(${PROJECT_NAME} INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(${PROJECT_NAME} INTERFACE ${WINDLIGHT_DEPS})
