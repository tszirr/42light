#pragma once

// https://github.com/glfw/glfw/blob/master/include/GLFW/glfw3.h

/* Printable keys */
#define WLGT_KEY_UNKNOWN            -1

/* Printable keys */
#define WLGT_KEY_SPACE              32
#define WLGT_KEY_APOSTROPHE         39  /* ' */
#define WLGT_KEY_COMMA              44  /* , */
#define WLGT_KEY_MINUS              45  /* - */
#define WLGT_KEY_PERIOD             46  /* . */
#define WLGT_KEY_SLASH              47  /* / */
#define WLGT_KEY_0                  48
#define WLGT_KEY_1                  49
#define WLGT_KEY_2                  50
#define WLGT_KEY_3                  51
#define WLGT_KEY_4                  52
#define WLGT_KEY_5                  53
#define WLGT_KEY_6                  54
#define WLGT_KEY_7                  55
#define WLGT_KEY_8                  56
#define WLGT_KEY_9                  57
#define WLGT_KEY_SEMICOLON          59  /* ; */
#define WLGT_KEY_EQUAL              61  /* = */
#define WLGT_KEY_A                  65
#define WLGT_KEY_B                  66
#define WLGT_KEY_C                  67
#define WLGT_KEY_D                  68
#define WLGT_KEY_E                  69
#define WLGT_KEY_F                  70
#define WLGT_KEY_G                  71
#define WLGT_KEY_H                  72
#define WLGT_KEY_I                  73
#define WLGT_KEY_J                  74
#define WLGT_KEY_K                  75
#define WLGT_KEY_L                  76
#define WLGT_KEY_M                  77
#define WLGT_KEY_N                  78
#define WLGT_KEY_O                  79
#define WLGT_KEY_P                  80
#define WLGT_KEY_Q                  81
#define WLGT_KEY_R                  82
#define WLGT_KEY_S                  83
#define WLGT_KEY_T                  84
#define WLGT_KEY_U                  85
#define WLGT_KEY_V                  86
#define WLGT_KEY_W                  87
#define WLGT_KEY_X                  88
#define WLGT_KEY_Y                  89
#define WLGT_KEY_Z                  90
#define WLGT_KEY_LEFT_BRACKET       91  /* [ */
#define WLGT_KEY_BACKSLASH          92  /* \ */
#define WLGT_KEY_RIGHT_BRACKET      93  /* ] */
#define WLGT_KEY_GRAVE_ACCENT       96  /* ` */
#define WLGT_KEY_WORLD_1            161 /* non-US #1 */
#define WLGT_KEY_WORLD_2            162 /* non-US #2 */

/* Function keys */
#define WLGT_KEY_ESCAPE             256
#define WLGT_KEY_ENTER              257
#define WLGT_KEY_TAB                258
#define WLGT_KEY_BACKSPACE          259
#define WLGT_KEY_INSERT             260
#define WLGT_KEY_DELETE             261
#define WLGT_KEY_RIGHT              262
#define WLGT_KEY_LEFT               263
#define WLGT_KEY_DOWN               264
#define WLGT_KEY_UP                 265
#define WLGT_KEY_PAGE_UP            266
#define WLGT_KEY_PAGE_DOWN          267
#define WLGT_KEY_HOME               268
#define WLGT_KEY_END                269
#define WLGT_KEY_CAPS_LOCK          280
#define WLGT_KEY_SCROLL_LOCK        281
#define WLGT_KEY_NUM_LOCK           282
#define WLGT_KEY_PRINT_SCREEN       283
#define WLGT_KEY_PAUSE              284
#define WLGT_KEY_F1                 290
#define WLGT_KEY_F2                 291
#define WLGT_KEY_F3                 292
#define WLGT_KEY_F4                 293
#define WLGT_KEY_F5                 294
#define WLGT_KEY_F6                 295
#define WLGT_KEY_F7                 296
#define WLGT_KEY_F8                 297
#define WLGT_KEY_F9                 298
#define WLGT_KEY_F10                299
#define WLGT_KEY_F11                300
#define WLGT_KEY_F12                301
#define WLGT_KEY_F13                302
#define WLGT_KEY_F14                303
#define WLGT_KEY_F15                304
#define WLGT_KEY_F16                305
#define WLGT_KEY_F17                306
#define WLGT_KEY_F18                307
#define WLGT_KEY_F19                308
#define WLGT_KEY_F20                309
#define WLGT_KEY_F21                310
#define WLGT_KEY_F22                311
#define WLGT_KEY_F23                312
#define WLGT_KEY_F24                313
#define WLGT_KEY_F25                314
#define WLGT_KEY_KP_0               320
#define WLGT_KEY_KP_1               321
#define WLGT_KEY_KP_2               322
#define WLGT_KEY_KP_3               323
#define WLGT_KEY_KP_4               324
#define WLGT_KEY_KP_5               325
#define WLGT_KEY_KP_6               326
#define WLGT_KEY_KP_7               327
#define WLGT_KEY_KP_8               328
#define WLGT_KEY_KP_9               329
#define WLGT_KEY_KP_DECIMAL         330
#define WLGT_KEY_KP_DIVIDE          331
#define WLGT_KEY_KP_MULTIPLY        332
#define WLGT_KEY_KP_SUBTRACT        333
#define WLGT_KEY_KP_ADD             334
#define WLGT_KEY_KP_ENTER           335
#define WLGT_KEY_KP_EQUAL           336
#define WLGT_KEY_LEFT_SHIFT         340
#define WLGT_KEY_LEFT_CONTROL       341
#define WLGT_KEY_LEFT_ALT           342
#define WLGT_KEY_LEFT_SUPER         343
#define WLGT_KEY_RIGHT_SHIFT        344
#define WLGT_KEY_RIGHT_CONTROL      345
#define WLGT_KEY_RIGHT_ALT          346
#define WLGT_KEY_RIGHT_SUPER        347
#define WLGT_KEY_MENU               348

#define WLGT_KEY_LAST WLGT_KEY_MENU
