#pragma once

#define XK_LATIN1
#define XK_MISCELLANY
#include <X11/keysymdef.h>
#include <X11/XKBlib.h>
#include "string.h"

#define WLGT_KEYCODE_COUNT 512
short wlgt_keycodes[WLGT_KEYCODE_COUNT] = { };
short wlgt_scancodes[WLGT_KEY_LAST + 1] = { };

static void wlgt_init_keys() {
    memset(wlgt_keycodes, WLGT_KEY_UNKNOWN, sizeof(wlgt_keycodes));
    memset(wlgt_scancodes, WLGT_KEY_UNKNOWN, sizeof(wlgt_scancodes));

	wlgt_keycodes[0xff + (0xff & XK_Escape)]         = WLGT_KEY_ESCAPE;
	wlgt_keycodes[0xff + (0xff & XK_Tab)]            = WLGT_KEY_TAB;
	wlgt_keycodes[0xff + (0xff & XK_Shift_L)]        = WLGT_KEY_LEFT_SHIFT;
	wlgt_keycodes[0xff + (0xff & XK_Shift_R)]        = WLGT_KEY_RIGHT_SHIFT;
	wlgt_keycodes[0xff + (0xff & XK_Control_L)]      = WLGT_KEY_LEFT_CONTROL;
	wlgt_keycodes[0xff + (0xff & XK_Control_R)]      = WLGT_KEY_RIGHT_CONTROL;
	wlgt_keycodes[0xff + (0xff & XK_Meta_L)]         = WLGT_KEY_LEFT_ALT;
	wlgt_keycodes[0xff + (0xff & XK_Alt_L)]          = WLGT_KEY_LEFT_ALT;
	wlgt_keycodes[0xff + (0xff & XK_Mode_switch)]    = WLGT_KEY_RIGHT_ALT;
	wlgt_keycodes[0xff + (0xff & XK_ISO_Level3_Shift)] = WLGT_KEY_RIGHT_ALT;
	wlgt_keycodes[0xff + (0xff & XK_Meta_R)]         = WLGT_KEY_RIGHT_ALT;
	wlgt_keycodes[0xff + (0xff & XK_Alt_R)]          = WLGT_KEY_RIGHT_ALT;
	wlgt_keycodes[0xff + (0xff & XK_Super_L)]        = WLGT_KEY_LEFT_SUPER;
	wlgt_keycodes[0xff + (0xff & XK_Super_R)]        = WLGT_KEY_RIGHT_SUPER;
	wlgt_keycodes[0xff + (0xff & XK_Menu)]           = WLGT_KEY_MENU;
	wlgt_keycodes[0xff + (0xff & XK_Num_Lock)]       = WLGT_KEY_NUM_LOCK;
	wlgt_keycodes[0xff + (0xff & XK_Caps_Lock)]      = WLGT_KEY_CAPS_LOCK;
	wlgt_keycodes[0xff + (0xff & XK_Print)]          = WLGT_KEY_PRINT_SCREEN;
	wlgt_keycodes[0xff + (0xff & XK_Scroll_Lock)]    = WLGT_KEY_SCROLL_LOCK;
	wlgt_keycodes[0xff + (0xff & XK_Pause)]          = WLGT_KEY_PAUSE;
	wlgt_keycodes[0xff + (0xff & XK_Delete)]         = WLGT_KEY_DELETE;
	wlgt_keycodes[0xff + (0xff & XK_BackSpace)]      = WLGT_KEY_BACKSPACE;
	wlgt_keycodes[0xff + (0xff & XK_Return)]         = WLGT_KEY_ENTER;
	wlgt_keycodes[0xff + (0xff & XK_Home)]           = WLGT_KEY_HOME;
	wlgt_keycodes[0xff + (0xff & XK_End)]            = WLGT_KEY_END;
	wlgt_keycodes[0xff + (0xff & XK_Page_Up)]        = WLGT_KEY_PAGE_UP;
	wlgt_keycodes[0xff + (0xff & XK_Page_Down)]      = WLGT_KEY_PAGE_DOWN;
	wlgt_keycodes[0xff + (0xff & XK_Insert)]         = WLGT_KEY_INSERT;
	wlgt_keycodes[0xff + (0xff & XK_Left)]           = WLGT_KEY_LEFT;
	wlgt_keycodes[0xff + (0xff & XK_Right)]          = WLGT_KEY_RIGHT;
	wlgt_keycodes[0xff + (0xff & XK_Down)]           = WLGT_KEY_DOWN;
	wlgt_keycodes[0xff + (0xff & XK_Up)]             = WLGT_KEY_UP;
	wlgt_keycodes[0xff + (0xff & XK_F1)]             = WLGT_KEY_F1;
	wlgt_keycodes[0xff + (0xff & XK_F2)]             = WLGT_KEY_F2;
	wlgt_keycodes[0xff + (0xff & XK_F3)]             = WLGT_KEY_F3;
	wlgt_keycodes[0xff + (0xff & XK_F4)]             = WLGT_KEY_F4;
	wlgt_keycodes[0xff + (0xff & XK_F5)]             = WLGT_KEY_F5;
	wlgt_keycodes[0xff + (0xff & XK_F6)]             = WLGT_KEY_F6;
	wlgt_keycodes[0xff + (0xff & XK_F7)]             = WLGT_KEY_F7;
	wlgt_keycodes[0xff + (0xff & XK_F8)]             = WLGT_KEY_F8;
	wlgt_keycodes[0xff + (0xff & XK_F9)]             = WLGT_KEY_F9;
	wlgt_keycodes[0xff + (0xff & XK_F10)]            = WLGT_KEY_F10;
	wlgt_keycodes[0xff + (0xff & XK_F11)]            = WLGT_KEY_F11;
	wlgt_keycodes[0xff + (0xff & XK_F12)]            = WLGT_KEY_F12;
	wlgt_keycodes[0xff + (0xff & XK_F13)]            = WLGT_KEY_F13;
	wlgt_keycodes[0xff + (0xff & XK_F14)]            = WLGT_KEY_F14;
	wlgt_keycodes[0xff + (0xff & XK_F15)]            = WLGT_KEY_F15;
	wlgt_keycodes[0xff + (0xff & XK_F16)]            = WLGT_KEY_F16;
	wlgt_keycodes[0xff + (0xff & XK_F17)]            = WLGT_KEY_F17;
	wlgt_keycodes[0xff + (0xff & XK_F18)]            = WLGT_KEY_F18;
	wlgt_keycodes[0xff + (0xff & XK_F19)]            = WLGT_KEY_F19;
	wlgt_keycodes[0xff + (0xff & XK_F20)]            = WLGT_KEY_F20;
	wlgt_keycodes[0xff + (0xff & XK_F21)]            = WLGT_KEY_F21;
	wlgt_keycodes[0xff + (0xff & XK_F22)]            = WLGT_KEY_F22;
	wlgt_keycodes[0xff + (0xff & XK_F23)]            = WLGT_KEY_F23;
	wlgt_keycodes[0xff + (0xff & XK_F24)]            = WLGT_KEY_F24;
	wlgt_keycodes[0xff + (0xff & XK_F25)]            = WLGT_KEY_F25;
	wlgt_keycodes[0xff + (0xff & XK_KP_Divide)]      = WLGT_KEY_KP_DIVIDE;
	wlgt_keycodes[0xff + (0xff & XK_KP_Multiply)]    = WLGT_KEY_KP_MULTIPLY;
	wlgt_keycodes[0xff + (0xff & XK_KP_Subtract)]    = WLGT_KEY_KP_SUBTRACT;
	wlgt_keycodes[0xff + (0xff & XK_KP_Add)]         = WLGT_KEY_KP_ADD;
	wlgt_keycodes[0xff + (0xff & XK_KP_Insert)]      = WLGT_KEY_KP_0;
	wlgt_keycodes[0xff + (0xff & XK_KP_End)]         = WLGT_KEY_KP_1;
	wlgt_keycodes[0xff + (0xff & XK_KP_Down)]        = WLGT_KEY_KP_2;
	wlgt_keycodes[0xff + (0xff & XK_KP_Page_Down)]   = WLGT_KEY_KP_3;
	wlgt_keycodes[0xff + (0xff & XK_KP_Left)]        = WLGT_KEY_KP_4;
	wlgt_keycodes[0xff + (0xff & XK_KP_Right)]       = WLGT_KEY_KP_6;
	wlgt_keycodes[0xff + (0xff & XK_KP_Home)]        = WLGT_KEY_KP_7;
	wlgt_keycodes[0xff + (0xff & XK_KP_Up)]          = WLGT_KEY_KP_8;
	wlgt_keycodes[0xff + (0xff & XK_KP_Page_Up)]     = WLGT_KEY_KP_9;
	wlgt_keycodes[0xff + (0xff & XK_KP_Delete)]      = WLGT_KEY_KP_DECIMAL;
	wlgt_keycodes[0xff + (0xff & XK_KP_Equal)]       = WLGT_KEY_KP_EQUAL;
	wlgt_keycodes[0xff + (0xff & XK_KP_Enter)]       = WLGT_KEY_KP_ENTER;
	wlgt_keycodes[XK_a]              = WLGT_KEY_A;
	wlgt_keycodes[XK_b]              = WLGT_KEY_B;
	wlgt_keycodes[XK_c]              = WLGT_KEY_C;
	wlgt_keycodes[XK_d]              = WLGT_KEY_D;
	wlgt_keycodes[XK_e]              = WLGT_KEY_E;
	wlgt_keycodes[XK_f]              = WLGT_KEY_F;
	wlgt_keycodes[XK_g]              = WLGT_KEY_G;
	wlgt_keycodes[XK_h]              = WLGT_KEY_H;
	wlgt_keycodes[XK_i]              = WLGT_KEY_I;
	wlgt_keycodes[XK_j]              = WLGT_KEY_J;
	wlgt_keycodes[XK_k]              = WLGT_KEY_K;
	wlgt_keycodes[XK_l]              = WLGT_KEY_L;
	wlgt_keycodes[XK_m]              = WLGT_KEY_M;
	wlgt_keycodes[XK_n]              = WLGT_KEY_N;
	wlgt_keycodes[XK_o]              = WLGT_KEY_O;
	wlgt_keycodes[XK_p]              = WLGT_KEY_P;
	wlgt_keycodes[XK_q]              = WLGT_KEY_Q;
	wlgt_keycodes[XK_r]              = WLGT_KEY_R;
	wlgt_keycodes[XK_s]              = WLGT_KEY_S;
	wlgt_keycodes[XK_t]              = WLGT_KEY_T;
	wlgt_keycodes[XK_u]              = WLGT_KEY_U;
	wlgt_keycodes[XK_v]              = WLGT_KEY_V;
	wlgt_keycodes[XK_w]              = WLGT_KEY_W;
	wlgt_keycodes[XK_x]              = WLGT_KEY_X;
	wlgt_keycodes[XK_y]              = WLGT_KEY_Y;
	wlgt_keycodes[XK_z]              = WLGT_KEY_Z;
	wlgt_keycodes[XK_1]              = WLGT_KEY_1;
	wlgt_keycodes[XK_2]              = WLGT_KEY_2;
	wlgt_keycodes[XK_3]              = WLGT_KEY_3;
	wlgt_keycodes[XK_4]              = WLGT_KEY_4;
	wlgt_keycodes[XK_5]              = WLGT_KEY_5;
	wlgt_keycodes[XK_6]              = WLGT_KEY_6;
	wlgt_keycodes[XK_7]              = WLGT_KEY_7;
	wlgt_keycodes[XK_8]              = WLGT_KEY_8;
	wlgt_keycodes[XK_9]              = WLGT_KEY_9;
	wlgt_keycodes[XK_0]              = WLGT_KEY_0;
	wlgt_keycodes[XK_space]          = WLGT_KEY_SPACE;
	wlgt_keycodes[XK_minus]          = WLGT_KEY_MINUS;
	wlgt_keycodes[XK_equal]          = WLGT_KEY_EQUAL;
	wlgt_keycodes[XK_bracketleft]    = WLGT_KEY_LEFT_BRACKET;
	wlgt_keycodes[XK_bracketright]   = WLGT_KEY_RIGHT_BRACKET;
	wlgt_keycodes[XK_backslash]      = WLGT_KEY_BACKSLASH;
	wlgt_keycodes[XK_semicolon]      = WLGT_KEY_SEMICOLON;
	wlgt_keycodes[XK_apostrophe]     = WLGT_KEY_APOSTROPHE;
	wlgt_keycodes[XK_grave]          = WLGT_KEY_GRAVE_ACCENT;
	wlgt_keycodes[XK_comma]          = WLGT_KEY_COMMA;
	wlgt_keycodes[XK_period]         = WLGT_KEY_PERIOD;
	wlgt_keycodes[XK_slash]          = WLGT_KEY_SLASH;
	wlgt_keycodes[XK_less]           = WLGT_KEY_WORLD_1;
	
    for (int scancode = 0; scancode < WLGT_KEYCODE_COUNT; scancode++) {
        if (wlgt_keycodes[scancode] > 0)
            wlgt_scancodes[wlgt_keycodes[scancode]] = scancode;
    }
}

int wlgt_translate_key(KeySym keySym, Time time) {
	const int hiSym = (keySym & 0xff00) >> 8;
	const int loSym = keySym & 0xff;
	return wlgt_keycodes[loSym + hiSym];
}
