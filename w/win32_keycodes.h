#pragma once

#include "string.h"

#define WLGT_KEYCODE_COUNT 512
short wlgt_keycodes[WLGT_KEYCODE_COUNT] = { 0 };
short wlgt_scancodes[WLGT_KEY_LAST + 1] = { 0 };

// https://github.com/glfw/glfw/blob/master/include/GLFW/glfw3.h
static void wlgt_init_keys() {
    memset(wlgt_keycodes, -1, sizeof(wlgt_keycodes));
    memset(wlgt_scancodes, -1, sizeof(wlgt_scancodes));

    wlgt_keycodes[0x00B] = WLGT_KEY_0;
    wlgt_keycodes[0x002] = WLGT_KEY_1;
    wlgt_keycodes[0x003] = WLGT_KEY_2;
    wlgt_keycodes[0x004] = WLGT_KEY_3;
    wlgt_keycodes[0x005] = WLGT_KEY_4;
    wlgt_keycodes[0x006] = WLGT_KEY_5;
    wlgt_keycodes[0x007] = WLGT_KEY_6;
    wlgt_keycodes[0x008] = WLGT_KEY_7;
    wlgt_keycodes[0x009] = WLGT_KEY_8;
    wlgt_keycodes[0x00A] = WLGT_KEY_9;
    wlgt_keycodes[0x01E] = WLGT_KEY_A;
    wlgt_keycodes[0x030] = WLGT_KEY_B;
    wlgt_keycodes[0x02E] = WLGT_KEY_C;
    wlgt_keycodes[0x020] = WLGT_KEY_D;
    wlgt_keycodes[0x012] = WLGT_KEY_E;
    wlgt_keycodes[0x021] = WLGT_KEY_F;
    wlgt_keycodes[0x022] = WLGT_KEY_G;
    wlgt_keycodes[0x023] = WLGT_KEY_H;
    wlgt_keycodes[0x017] = WLGT_KEY_I;
    wlgt_keycodes[0x024] = WLGT_KEY_J;
    wlgt_keycodes[0x025] = WLGT_KEY_K;
    wlgt_keycodes[0x026] = WLGT_KEY_L;
    wlgt_keycodes[0x032] = WLGT_KEY_M;
    wlgt_keycodes[0x031] = WLGT_KEY_N;
    wlgt_keycodes[0x018] = WLGT_KEY_O;
    wlgt_keycodes[0x019] = WLGT_KEY_P;
    wlgt_keycodes[0x010] = WLGT_KEY_Q;
    wlgt_keycodes[0x013] = WLGT_KEY_R;
    wlgt_keycodes[0x01F] = WLGT_KEY_S;
    wlgt_keycodes[0x014] = WLGT_KEY_T;
    wlgt_keycodes[0x016] = WLGT_KEY_U;
    wlgt_keycodes[0x02F] = WLGT_KEY_V;
    wlgt_keycodes[0x011] = WLGT_KEY_W;
    wlgt_keycodes[0x02D] = WLGT_KEY_X;
    wlgt_keycodes[0x015] = WLGT_KEY_Y;
    wlgt_keycodes[0x02C] = WLGT_KEY_Z;

    wlgt_keycodes[0x028] = WLGT_KEY_APOSTROPHE;
    wlgt_keycodes[0x02B] = WLGT_KEY_BACKSLASH;
    wlgt_keycodes[0x033] = WLGT_KEY_COMMA;
    wlgt_keycodes[0x00D] = WLGT_KEY_EQUAL;
    wlgt_keycodes[0x029] = WLGT_KEY_GRAVE_ACCENT;
    wlgt_keycodes[0x01A] = WLGT_KEY_LEFT_BRACKET;
    wlgt_keycodes[0x00C] = WLGT_KEY_MINUS;
    wlgt_keycodes[0x034] = WLGT_KEY_PERIOD;
    wlgt_keycodes[0x01B] = WLGT_KEY_RIGHT_BRACKET;
    wlgt_keycodes[0x027] = WLGT_KEY_SEMICOLON;
    wlgt_keycodes[0x035] = WLGT_KEY_SLASH;
    wlgt_keycodes[0x056] = WLGT_KEY_WORLD_2;

    wlgt_keycodes[0x00E] = WLGT_KEY_BACKSPACE;
    wlgt_keycodes[0x153] = WLGT_KEY_DELETE;
    wlgt_keycodes[0x14F] = WLGT_KEY_END;
    wlgt_keycodes[0x01C] = WLGT_KEY_ENTER;
    wlgt_keycodes[0x001] = WLGT_KEY_ESCAPE;
    wlgt_keycodes[0x147] = WLGT_KEY_HOME;
    wlgt_keycodes[0x152] = WLGT_KEY_INSERT;
    wlgt_keycodes[0x15D] = WLGT_KEY_MENU;
    wlgt_keycodes[0x151] = WLGT_KEY_PAGE_DOWN;
    wlgt_keycodes[0x149] = WLGT_KEY_PAGE_UP;
    wlgt_keycodes[0x045] = WLGT_KEY_PAUSE;
    wlgt_keycodes[0x146] = WLGT_KEY_PAUSE;
    wlgt_keycodes[0x039] = WLGT_KEY_SPACE;
    wlgt_keycodes[0x00F] = WLGT_KEY_TAB;
    wlgt_keycodes[0x03A] = WLGT_KEY_CAPS_LOCK;
    wlgt_keycodes[0x145] = WLGT_KEY_NUM_LOCK;
    wlgt_keycodes[0x046] = WLGT_KEY_SCROLL_LOCK;
    wlgt_keycodes[0x03B] = WLGT_KEY_F1;
    wlgt_keycodes[0x03C] = WLGT_KEY_F2;
    wlgt_keycodes[0x03D] = WLGT_KEY_F3;
    wlgt_keycodes[0x03E] = WLGT_KEY_F4;
    wlgt_keycodes[0x03F] = WLGT_KEY_F5;
    wlgt_keycodes[0x040] = WLGT_KEY_F6;
    wlgt_keycodes[0x041] = WLGT_KEY_F7;
    wlgt_keycodes[0x042] = WLGT_KEY_F8;
    wlgt_keycodes[0x043] = WLGT_KEY_F9;
    wlgt_keycodes[0x044] = WLGT_KEY_F10;
    wlgt_keycodes[0x057] = WLGT_KEY_F11;
    wlgt_keycodes[0x058] = WLGT_KEY_F12;
    wlgt_keycodes[0x064] = WLGT_KEY_F13;
    wlgt_keycodes[0x065] = WLGT_KEY_F14;
    wlgt_keycodes[0x066] = WLGT_KEY_F15;
    wlgt_keycodes[0x067] = WLGT_KEY_F16;
    wlgt_keycodes[0x068] = WLGT_KEY_F17;
    wlgt_keycodes[0x069] = WLGT_KEY_F18;
    wlgt_keycodes[0x06A] = WLGT_KEY_F19;
    wlgt_keycodes[0x06B] = WLGT_KEY_F20;
    wlgt_keycodes[0x06C] = WLGT_KEY_F21;
    wlgt_keycodes[0x06D] = WLGT_KEY_F22;
    wlgt_keycodes[0x06E] = WLGT_KEY_F23;
    wlgt_keycodes[0x076] = WLGT_KEY_F24;
    wlgt_keycodes[0x038] = WLGT_KEY_LEFT_ALT;
    wlgt_keycodes[0x01D] = WLGT_KEY_LEFT_CONTROL;
    wlgt_keycodes[0x02A] = WLGT_KEY_LEFT_SHIFT;
    wlgt_keycodes[0x15B] = WLGT_KEY_LEFT_SUPER;
    wlgt_keycodes[0x137] = WLGT_KEY_PRINT_SCREEN;
    wlgt_keycodes[0x138] = WLGT_KEY_RIGHT_ALT;
    wlgt_keycodes[0x11D] = WLGT_KEY_RIGHT_CONTROL;
    wlgt_keycodes[0x036] = WLGT_KEY_RIGHT_SHIFT;
    wlgt_keycodes[0x15C] = WLGT_KEY_RIGHT_SUPER;
    wlgt_keycodes[0x150] = WLGT_KEY_DOWN;
    wlgt_keycodes[0x14B] = WLGT_KEY_LEFT;
    wlgt_keycodes[0x14D] = WLGT_KEY_RIGHT;
    wlgt_keycodes[0x148] = WLGT_KEY_UP;

    wlgt_keycodes[0x052] = WLGT_KEY_KP_0;
    wlgt_keycodes[0x04F] = WLGT_KEY_KP_1;
    wlgt_keycodes[0x050] = WLGT_KEY_KP_2;
    wlgt_keycodes[0x051] = WLGT_KEY_KP_3;
    wlgt_keycodes[0x04B] = WLGT_KEY_KP_4;
    wlgt_keycodes[0x04C] = WLGT_KEY_KP_5;
    wlgt_keycodes[0x04D] = WLGT_KEY_KP_6;
    wlgt_keycodes[0x047] = WLGT_KEY_KP_7;
    wlgt_keycodes[0x048] = WLGT_KEY_KP_8;
    wlgt_keycodes[0x049] = WLGT_KEY_KP_9;
    wlgt_keycodes[0x04E] = WLGT_KEY_KP_ADD;
    wlgt_keycodes[0x053] = WLGT_KEY_KP_DECIMAL;
    wlgt_keycodes[0x135] = WLGT_KEY_KP_DIVIDE;
    wlgt_keycodes[0x11C] = WLGT_KEY_KP_ENTER;
    wlgt_keycodes[0x037] = WLGT_KEY_KP_MULTIPLY;
    wlgt_keycodes[0x04A] = WLGT_KEY_KP_SUBTRACT;

    for (int scancode = 0; scancode < WLGT_KEYCODE_COUNT; scancode++) {
        if (wlgt_keycodes[scancode] > 0)
            wlgt_scancodes[wlgt_keycodes[scancode]] = scancode;
    }
}

int wlgt_translate_key(WPARAM wParam, LPARAM lParam, LONG time) {
	// The Ctrl keys require special handling
	if (wParam == VK_CONTROL) {
		// Right side keys have the extended key bit set
		if (lParam & 0x01000000)
			return WLGT_KEY_RIGHT_CONTROL;

		// HACK: Alt Gr sends Left Ctrl and then Right Alt in close sequence
		//       We only want the Right Alt message, so if the next message is
		//       Right Alt we ignore this (synthetic) Left Ctrl message
		MSG next;
		if (PeekMessageW(&next, NULL, 0, 0, PM_NOREMOVE)) {
			if (next.message == WM_KEYDOWN || next.message == WM_SYSKEYDOWN ||
				next.message == WM_KEYUP || next.message == WM_SYSKEYUP) {
				if (next.wParam == VK_MENU && (next.lParam & 0x01000000) && next.time == time)
					// Next message is Right Alt down so discard this
					return -1;
			}
		}

		return WLGT_KEY_LEFT_CONTROL;
	}

	if (wParam == VK_PROCESSKEY) {
		// IME notifies that keys have been filtered by setting the virtual
		// key-code to VK_PROCESSKEY
		return -1;
	}

	return wlgt_keycodes[HIWORD(lParam) & 0x1FF];
}
