#include "window.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include "keycodes.h"
#include "x11_keycodes.h"
#include <pthread.h>
#include <sys/select.h>
#include <errno.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <stdlib.h>
#include <stdio.h>

#define tmalloc(t, n) ((t*) malloc(sizeof(t) * n))

struct ThreadData {
	void(*run)(void*);
	void* user;
	bool(*init)(void*);
	bool initialized;
	bool running;
	pthread_mutex_t mtx;
	pthread_cond_t evt;
};
static void* threadProc(void* lpParameter) {
	ThreadData* data = (ThreadData*)lpParameter;
	void(*run)(void*) = data->run;
	void* user = data->user;

	if (data->init) {
		bool initialized;
		try { initialized = data->init(data->user); }
		catch (...) {
			initialized = false;
		}
		pthread_mutex_lock(&data->mtx);
		data->initialized = initialized;
		data->running = true;
		pthread_cond_signal(&data->evt);
		pthread_mutex_unlock(&data->mtx);
		if (!initialized)
			return (void*) (ptrdiff_t) -1;
	}
	else
		free(data);
	
	run(user);
	return 0;
}
bool create_thread(void(*run)(void*), void* user, bool(*init)(void*)) {
	ThreadData data = { run, user, init, false, !init };
	if (init) {
		pthread_mutex_init(&data.mtx, 0);
		pthread_cond_init(&data.evt, 0);
	}
	
	bool started;
	{
		ThreadData* sdata = &data;
		if (!init) {
			sdata = tmalloc(ThreadData, 1);
			memcpy(sdata, &data, sizeof(data));
		}
		pthread_t thread;
		started = !pthread_create(&thread, NULL, threadProc, sdata);
		if (!started && sdata != &data)
			free(sdata);
	}
	
	if (init) {
		if (started) {
			pthread_mutex_lock(&data.mtx);
			while (!data.running)
				pthread_cond_wait(&data.evt, &data.mtx);
			pthread_mutex_unlock(&data.mtx);
			started = true;
		}
		pthread_cond_destroy(&data.evt);
		pthread_mutex_destroy(&data.mtx);
	}
	return started;
}

struct wlgt_context {
	Display* display;
	XContext context;
	Atom wmProtocols;
	union {
		struct { Atom wmDeleteWindow, wmPing, wmQuit; };
		Atom extensions[2];
	};
	Window root;
	Time time;
	mqd_t updateQueueIn;
	mqd_t updateQueueOut;
} wlgtx11 = { };

static bool close_display() {	
	mq_close(wlgtx11.updateQueueIn);
	mq_close(wlgtx11.updateQueueOut);

	return wlgtx11.display && !XCloseDisplay(wlgtx11.display);
}
static bool open_display() {
	if (!XInitThreads())
		printf("No X11 threading support\n");
	
	wlgtx11.display = XOpenDisplay(0);
	
	if (!wlgtx11.display)
		return false; // todo: error

	wlgtx11.root = DefaultRootWindow(wlgtx11.display);
	wlgtx11.wmProtocols = XInternAtom(wlgtx11.display, "WM_PROTOCOLS", False);
	wlgtx11.wmDeleteWindow = XInternAtom(wlgtx11.display, "WM_DELETE_WINDOW", False);
	wlgtx11.wmPing = XInternAtom(wlgtx11.display, "_NET_WM_PING", False);
	wlgtx11.wmQuit = XInternAtom(wlgtx11.display, "WM_WLGT_QUIT", False);

	wlgtx11.context = XUniqueContext();
	
	char msgQname[512];
	sprintf(msgQname, "/wlgtx11.updateQueue.%p", (void*) wlgtx11.display);

	// get default queue length
	struct mq_attr Qattr = { O_NONBLOCK };
	{
		mqd_t tmpq = mq_open(msgQname, O_WRONLY | O_CREAT | O_NONBLOCK, S_IRWXU, 0);
		mq_unlink(msgQname);
		mq_getattr(tmpq, &Qattr);
		mq_close(tmpq);
	}
	Qattr.mq_msgsize = sizeof(void*);

	wlgtx11.updateQueueOut = mq_open(msgQname, O_WRONLY | O_CREAT | O_NONBLOCK, S_IRWXU, &Qattr);
	wlgtx11.updateQueueIn = mq_open(msgQname, O_RDONLY | O_NONBLOCK);
	mq_unlink(msgQname);
	
	return true;
}

struct WindowBitmap {
	unsigned char* bitmap;
	XImage* image;
};
static int const F_LAYERED = 0x1;
static int const F_BUFFERED = 0x2;
static int const F_APPLICATION = 0x4;
struct WindowData {
	int cx, cy;
	int width;
	int height;
	void* user;
	int flags;
	wlgt_callbacks callbacks;
	WindowBitmap bmp;
	GC gc;
	short depth, bpp;	
};

static WindowData* window_data(Window handle) {
	WindowData* data = 0;
	XFindContext(wlgtx11.display, handle, wlgtx11.context, (XPointer*) &data);
	return data;
}
void* window_userdata(wlgt* wnd) {
	return window_data((Window) (ptrdiff_t) wnd)->user;
}

static void delete_offscreen_surface(WindowBitmap* bmp) {
	if (bmp->image)
		XDestroyImage(bmp->image);
	if (bmp->bitmap)
		free(bmp->bitmap);
	memset(bmp, 0, sizeof(WindowBitmap));
}
static void delete_offscreen_surface(Window hWnd, WindowData* data) {
	if (data->bmp.bitmap && data->callbacks.surface_changed) {
		XUnlockDisplay(wlgtx11.display);
		data->callbacks.surface_changed((wlgt*) (ptrdiff_t) hWnd, data->user, data->width, data->height, 0);
		XLockDisplay(wlgtx11.display);
	}
	delete_offscreen_surface(&data->bmp);
}

static void create_offscreen_surface(Window hWnd, WindowData* data, int width, int height) {	
	if (!width || !height) {
		XWindowAttributes attribs;
		XGetWindowAttributes(wlgtx11.display, hWnd, &attribs);
		width = attribs.width;
		height = attribs.height;
	}
	data->width = width;
	data->height = height;

	delete_offscreen_surface(hWnd, data);

	int bypp = (data->bpp + 7) / 8;
	data->bmp.bitmap = (unsigned char*) aligned_alloc(128, width * height * bypp);
	if (!data->bmp.bitmap)
		return;
	
	data->bmp.image = XCreateImage(wlgtx11.display, CopyFromParent, data->depth, ZPixmap, 0, 0,
		width, height, data->bpp, width * bypp);
	if (!data->bmp.image) {
		delete_offscreen_surface(&data->bmp);
		return;
	}
	
	data->bmp.image->byte_order = LSBFirst;
	
	if (data->callbacks.surface_changed) {
		XUnlockDisplay(wlgtx11.display);
		data->callbacks.surface_changed((wlgt*)hWnd, data->user, width, height, data->bmp.bitmap);
		XLockDisplay(wlgtx11.display);
	}
}

void async_update_window(wlgt* wnd) {
	mq_send(wlgtx11.updateQueueOut, (const char*) &wnd, sizeof(wnd), 0);
}

static void clean_update_queue(wlgt* rmwnd) {
		struct mq_attr Qattr = { O_NONBLOCK };
		mq_getattr(wlgtx11.updateQueueIn, &Qattr);
		for (int i = 0; i < Qattr.mq_curmsgs; ++i) {
				wlgt* wnd = 0;
				mq_receive(wlgtx11.updateQueueIn, (char*) &wnd, sizeof(wnd), 0);
				if (wnd && wnd != rmwnd) {
						mq_send(wlgtx11.updateQueueOut, (const char*) &wnd, sizeof(wnd), 0);
				}
		}
}

void update_window(wlgt* wnd) {
	XClearArea(wlgtx11.display, (Window) (ptrdiff_t) wnd, 0, 0, 0, 0, True);
}

void update_transparent_window(wlgt* wnd) {
	update_window(wnd);
}

static int wndProc(XEvent* event) {
	Window hWnd = event->xany.window;
	
	switch (event->type) {
	case ClientMessage:
		if (event->xclient.message_type == wlgtx11.wmProtocols) {
			const Atom protocol = event->xclient.data.l[0];
			
			if (protocol == wlgtx11.wmPing) {			
				XEvent reply = *event;
				reply.xclient.window = wlgtx11.root;
				XSendEvent(wlgtx11.display, wlgtx11.root,
						False,
						SubstructureNotifyMask | SubstructureRedirectMask,
						&reply);
			}
			else if (protocol == wlgtx11.wmDeleteWindow) {
				close_window((wlgt*) (ptrdiff_t) hWnd);
			}
		}
		break;
	
	case DestroyNotify:
		if (WindowData* data = window_data(hWnd)) {
			int quit = (data->flags & F_APPLICATION) != 0;
			if (data->callbacks.destroyed) {
				XUnlockDisplay(wlgtx11.display);
				quit = data->callbacks.destroyed((wlgt*) (ptrdiff_t) hWnd, data->user);
				XLockDisplay(wlgtx11.display);
			}
			XDeleteContext(wlgtx11.display, event->xany.window, wlgtx11.context);
			delete_offscreen_surface(hWnd, data);
			free(data);
			clean_update_queue((wlgt*) (ptrdiff_t) hWnd);
			// post quit message for other threads
			if (quit)
			{
				XEvent quitEvent;
				quitEvent.type = ClientMessage;
				quitEvent.xclient.message_type = wlgtx11.wmQuit;
				quitEvent.xclient.format = 32;
				quitEvent.xclient.data.l[0] = (int) hWnd;
				XSendEvent(wlgtx11.display, wlgtx11.root, False, SubstructureNotifyMask | SubstructureRedirectMask, &quitEvent);
			}
			return quit;
		}
		break;
	
/*	case WM_DISPLAYCHANGE: {
		WindowData* data = window_data(hWnd);
		if (data->flags & F_BUFFERED || data->bmp.bitmap)
			create_offscreen_surface(hWnd, data);
		break;
	}
*/	case CreateNotify:
		if (WindowData* data = window_data(hWnd)) {
			if (data->callbacks.created) {
				XUnlockDisplay(wlgtx11.display);
				data->callbacks.created((wlgt*) (ptrdiff_t) hWnd, data->user);
				XLockDisplay(wlgtx11.display);
			}
			data->cx = data->cy = -1;
		}
		// fall through
	case ConfigureNotify:
		if (WindowData* data = window_data(hWnd)) {
			bool sizeChanged = data->width != event->xconfigure.width || data->height != event->xconfigure.height;
			if (data->flags & F_BUFFERED || data->bmp.bitmap) {
				if (!data->bmp.bitmap || sizeChanged)
					create_offscreen_surface(hWnd, data, event->xconfigure.width, event->xconfigure.height);
			}
			if (sizeChanged) {
				data->width = event->xconfigure.width;
				data->height = event->xconfigure.height;
				if (data->callbacks.size_changed) {
					XUnlockDisplay(wlgtx11.display);
					data->callbacks.size_changed((wlgt*) (ptrdiff_t) hWnd, data->user, data->width, data->height);
					XLockDisplay(wlgtx11.display);
				}
			}

			if (data->callbacks.pos_changed) {
				XUnlockDisplay(wlgtx11.display);
				data->callbacks.pos_changed((wlgt*) (ptrdiff_t) hWnd, data->user, event->xconfigure.x, event->xconfigure.y, data->width, data->height);
				XLockDisplay(wlgtx11.display);
			}

//			if (data->flags & F_LAYERED)
//				InvalidateRect(hWnd, NULL, TRUE);
		}
		break;

	case Expose:
		if (WindowData* data = window_data(hWnd)) {
//			if (data->flags & F_LAYERED)
//				update_transparent_window((wlgt*)hWnd);
			if (data->bmp.bitmap) {
				data->bmp.image->data = (char*) data->bmp.bitmap;
				XPutImage(wlgtx11.display, hWnd, data->gc, data->bmp.image, 0, 0, 0, 0, data->width, data->height);
				data->bmp.image->data = NULL;
			}
		}
		break;

	case KeyPress:
	case KeyRelease:
		if (WindowData* data = window_data(hWnd)) {
			void (*callback)(wlgt*, void*, int, long long) = (event->type == KeyPress) ? data->callbacks.key_down : data->callbacks.key_up;
			if (callback) {
				const KeySym keySym = XkbKeycodeToKeysym(wlgtx11.display, event->xkey.keycode, 0, 0);
				int key = wlgt_translate_key(keySym, event->xkey.time);
				XUnlockDisplay(wlgtx11.display);
				callback((wlgt*) (ptrdiff_t) hWnd, data->user, key, (long long) event->xkey.time);
				XLockDisplay(wlgtx11.display);
			}
		}
		break;

	case ButtonPress:
	case ButtonRelease:
		if (WindowData* data = window_data(hWnd)) {
			void (*callback)(wlgt*,void*,int,int,int,float,float,long long) = event->type == ButtonPress ? data->callbacks.button_down : data->callbacks.button_up;
			if (callback) {
				int button = -1;
				switch (event->xbutton.button) {
				case Button1: button = wlgt_button1; break;
				case Button2: button = wlgt_button2; break;
				case Button3: button = wlgt_button3; break;
				}
				if (button != -1) {
					int x = event->xbutton.x;
					int y = event->xbutton.y;
					float fx = x / float(data->width);
					float fy = y / float(data->height);
					XUnlockDisplay(wlgtx11.display);
					callback((wlgt*) (ptrdiff_t) hWnd, data->user, button, x, y, fx, fy, (long long) event->xbutton.time);
					XLockDisplay(wlgtx11.display);
				}
			}
		}
		break;

	case LeaveNotify:
		if (WindowData* data = window_data(hWnd)) {
			data->cx = data->cy = -1;
		}
		break;

	case MotionNotify:
		if (WindowData* data = window_data(hWnd)) {
			if (data->callbacks.cursor_pos || data->callbacks.cursor_delta) {
				int buttons = 0;
				if (event->xmotion.state & Button1Mask) buttons |= wlgt_button1;
				if (event->xmotion.state & Button2Mask) buttons |= wlgt_button2;
				if (event->xmotion.state & Button3Mask) buttons |= wlgt_button3;
				int x = event->xmotion.x;
				int y = event->xmotion.y;
				if (data->callbacks.cursor_pos) {
					float fx = x / float(data->width);
					float fy = y / float(data->height);
					XUnlockDisplay(wlgtx11.display);
					data->callbacks.cursor_pos((wlgt*) (ptrdiff_t) hWnd, data->user, x, y, fx, fy, buttons, (long long) event->xmotion.time);
					XLockDisplay(wlgtx11.display);
				}
				if (data->callbacks.cursor_delta) {
					if (data->cx >= 0 && data->cy >= 0) {
						int dx = x - data->cx;
						int dy = y - data->cy;
						float fx = dx / float(data->width);
						float fy = dy / float(data->height);
						XUnlockDisplay(wlgtx11.display);
						data->callbacks.cursor_delta((wlgt*) (ptrdiff_t) hWnd, data->user, dx, dy, fx, fy, buttons, (long long) event->xmotion.time);
						XLockDisplay(wlgtx11.display);
					}
					data->cx = x;
					data->cy = y;
				}
			}
		}
		break;
	}

	return 0;
}

// https://github.com/glfw/glfw/blob/af866e05d20068be149e0cc410d64490fb0b7a8c/src/x11_window.c#L58
// Wait for data to arrive using select
// This avoids blocking other threads via the per-display Xlib lock that also
// covers GLX functions
//
static int wait_for_event() {
	fd_set fds;
	const int fdd = ConnectionNumber(wlgtx11.display);
	const int fdq = (int) wlgtx11.updateQueueIn;
	int count = (fdq > fdd ? fdq : fdd) + 1;

	for (;;) {
		FD_ZERO(&fds);
		FD_SET(fdd, &fds);
		FD_SET(fdq, &fds);

		if (select(count, &fds, NULL, NULL, NULL) != -1)
			return 1 + (int) !!FD_ISSET(fdq, &fds);
		else if (errno != EINTR)
			return 0;
	}

	// never
	return 0;
}

static void wlgt_update_time(XEvent* event) {
	Time time;
	switch (event->type) {
	case KeyPress:
	case KeyRelease:
		time = event->xkey.time;
		break;
	case EnterNotify:
	case LeaveNotify:
		time = event->xcrossing.time;
		break;
	case MotionNotify:
		time = event->xmotion.time;
		break;
	case ButtonPress:
	case ButtonRelease:
		time = event->xbutton.time;
		break;
	default:
		return;
	}
	wlgtx11.time = time;
}

static void pump_async_updates() {
	wlgt* wnd = 0, *lastUpdateWnd = 0;
	do {
			wnd = 0;
			mq_receive(wlgtx11.updateQueueIn, (char*) &wnd, sizeof(wnd), 0);
			if (wnd && wnd != lastUpdateWnd) {
					XEvent destroyEvent;
					if (XCheckTypedWindowEvent(wlgtx11.display, (Window) (ptrdiff_t) wnd, DestroyNotify, &destroyEvent))
							XPutBackEvent(wlgtx11.display, &destroyEvent);
					else {
							// combine update events so the X11 event queue is not flooded
							int updateEventMask = ExposureMask;
							XEvent event;
							if (XCheckWindowEvent(wlgtx11.display, (Window) (ptrdiff_t) wnd, updateEventMask, &event))
									// move to the end of the queue
									XSendEvent(wlgtx11.display, (Window) (ptrdiff_t) wnd, False, updateEventMask, &event);
							else
									// generate new
									update_window(wnd);
					}
					lastUpdateWnd = wnd;
			}
	}
	while (wnd);
}

bool wlgt_pump(bool wait) {
	XLockDisplay(wlgtx11.display);
	int count = XPending(wlgtx11.display);
	
	if (wait) {
		while (!count) {
			XUnlockDisplay(wlgtx11.display);
			
			// check for new X input and asynchronous update events
			int update = wait_for_event() - 1;
			
			XLockDisplay(wlgtx11.display);
			// potential asnyc update
			if (update > 0)
				pump_async_updates();
			count = XPending(wlgtx11.display);
		}
	}
	
	while (count--) {
		XEvent event;
		XNextEvent(wlgtx11.display, &event);
		
		wlgt_update_time(&event);
		
		int r = wndProc(&event);
		// confirmed delete message OR custom quit message from other threads quits application
		if (r && event.type == DestroyNotify || event.type == ClientMessage && event.xclient.message_type == wlgtx11.wmQuit) {
			XUnlockDisplay(wlgtx11.display);
			return false;
		}
	}

	XFlush(wlgtx11.display);
	XUnlockDisplay(wlgtx11.display);
	return true;
}

void wlgt_loop() {
	while (wlgt_pump(true));
}

long long wlgt_time() {
	return (long long) wlgtx11.time;
}

wlgt* create_window(char const* name, wlgt_rect const* rect, wlgt_flags const* flags, wlgt_callbacks const* callbacks, void* user, wlgt* owner) {
	XLockDisplay(wlgtx11.display);	

	int screen = DefaultScreen(wlgtx11.display);
	Visual* visual = DefaultVisual(wlgtx11.display, screen);
	Window parent = owner ? (Window) (ptrdiff_t) owner : wlgtx11.root;

	/*
	DWORD exStyle = 0;
	DWORD style = WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (flags->appwindow)
		exStyle |= WS_EX_APPWINDOW;

	if (flags->fullscreen)
		style |= WS_POPUP;
	else {
		if (flags->appwindow || flags->decorated)
			style |= WS_SYSMENU | WS_MINIMIZEBOX;
		if (flags->decorated) {
			style |= WS_CAPTION;
			if (flags->resizeable)
				style |= WS_MAXIMIZEBOX | WS_SIZEBOX;
		}
		else {
			style |= WS_POPUP;
			if (!flags->appwindow)
				exStyle |= WS_EX_TOOLWINDOW;
		}
	}

	if (flags->transparent) {
		exStyle |= WS_EX_LAYERED;
	}
	*/
	
	WindowData* data = tmalloc(WindowData, 1);
	memset(data, 0, sizeof(WindowData));
	memcpy(&data->callbacks, callbacks, sizeof(wlgt_callbacks));
//	if (flags->transparent)
//		data->flags |= F_LAYERED;
	if (!flags->bufferless)
		data->flags |= F_BUFFERED;
	if (flags->appwindow)
		data->flags |= F_APPLICATION;
	data->user = user;

	int left = rect->x;
	int top = rect->y;
	if (left == -1 && top == -1) {
		XWindowAttributes attribs;
		XGetWindowAttributes(wlgtx11.display, parent, &attribs);
		left = (attribs.width - rect->width) / 2;
		top = (attribs.height - rect->height) / 2;
	}

	data->gc = DefaultGC(wlgtx11.display, screen);
	data->depth = DefaultDepth(wlgtx11.display, screen);
	data->bpp = 32;
	// todo: check?
	
	XSetWindowAttributes attributes;
	attributes.event_mask = StructureNotifyMask | KeyPressMask | KeyReleaseMask |
			PointerMotionMask | ButtonPressMask | ButtonReleaseMask |
			ExposureMask | FocusChangeMask | VisibilityChangeMask |
			EnterWindowMask | LeaveWindowMask | PropertyChangeMask;
	Window handle = XCreateWindow(wlgtx11.display, parent, left, top, rect->width, rect->height, 0,
		data->depth, InputOutput, visual,
		CWEventMask, &attributes); // CWBackPixel | CWBorderPixel | CWBackingStore
	
	if (handle) {
		XSetWMProtocols(wlgtx11.display, handle, wlgtx11.extensions, sizeof(wlgtx11.extensions) / sizeof(wlgtx11.extensions[0]));
		int e = XSaveContext(wlgtx11.display, handle, wlgtx11.context, (XPointer) data);
		if (e) {
			XDestroyWindow(wlgtx11.display, handle);
			handle = 0;
		}
	}
	
	XUnlockDisplay(wlgtx11.display);

	if (!handle) {
		free(data);
		// todo: error handling!
		return NULL;
	}
	
	return (wlgt*) (ptrdiff_t) handle;
}

void show_window(wlgt* handle, bool show) {
	if (show)
		XMapWindow(wlgtx11.display, (Window) (ptrdiff_t) handle);
	else
		XUnmapWindow(wlgtx11.display, (Window) (ptrdiff_t) handle);
	XFlush(wlgtx11.display);
}

void update_window_pos(wlgt* wnd, wlgt_rect const* rect) {
	XWindowChanges c = { rect->x, rect->y, rect->width, rect->height };
	XConfigureWindow(wlgtx11.display, (Window) (ptrdiff_t) wnd, CWX | CWY | CWWidth | CWHeight, &c);
}

bool close_window(wlgt* wnd) {
	Window hWnd = (Window) (ptrdiff_t) wnd;
	if (WindowData* data = window_data(hWnd)) {
		if (data->callbacks.closed)
			if (!data->callbacks.closed(wnd, data->user))
				return false;
	}
	XDestroyWindow(wlgtx11.display, hWnd);
	clean_update_queue(wnd);
	return true;
}
void force_destroy_window(wlgt* wnd) {
	XDestroyWindow(wlgtx11.display, (Window) (ptrdiff_t) wnd);
	clean_update_queue(wnd);
}

static bool join_destroy_pump(Window hWnd) {
	while (true) {
		XEvent event;
		XNextEvent(wlgtx11.display, &event);
		wlgt_update_time(&event);
		wndProc(&event);
		// completion of destruction, quit message will end other threads
		if (event.type == DestroyNotify && event.xany.window == hWnd) {
			return false;
		}
	}

	// never
	return true;
}
bool join_close_window(wlgt* wnd) {
	XLockDisplay(wlgtx11.display);
	bool closed = close_window(wnd);
	if (closed) {
		join_destroy_pump((Window) (ptrdiff_t) wnd);
	}
	XUnlockDisplay(wlgtx11.display);
	return closed;
}
void join_force_destroy_window(wlgt* wnd) {
	XLockDisplay(wlgtx11.display);
	force_destroy_window(wnd);
	join_destroy_pump((Window) (ptrdiff_t) wnd);
	XUnlockDisplay(wlgtx11.display);
}

bool wlgt_init() {
	wlgt_init_keys();
	return open_display();
}

bool wlgt_uninit() {
	return close_display();
}
