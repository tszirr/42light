#pragma once

#ifdef WLGT_CPP
#	define WLGT_IN_C(...) 
#	define WLGT_IN_CPP(...) __VA_ARGS__
#else
#	define WLGT_IN_C(...) __VA_ARGS__
#	define WLGT_IN_CPP(...) 
#endif

bool wlgt_init();
bool wlgt_uninit();

bool wlgt_pump(bool wait_for_event);
void wlgt_loop();

long long wlgt_time();

struct wlgt;

struct wlgt_rect {
	int x WLGT_IN_CPP(= -1);
	int y WLGT_IN_CPP(= -1);
	int width WLGT_IN_CPP(= 800);
	int height WLGT_IN_CPP(= 600);

	WLGT_IN_CPP(wlgt_rect() { })
	WLGT_IN_CPP(wlgt_rect(int x, int y, int w, int h) : x(x), y(y), width(w), height(h) { })
};

struct wlgt_flags {
	bool appwindow WLGT_IN_CPP( = false);
	bool fullscreen WLGT_IN_CPP(= false);
	bool decorated WLGT_IN_CPP(= false);
	bool resizeable WLGT_IN_CPP(= false);
	bool transparent WLGT_IN_CPP(= false);
	bool bufferless WLGT_IN_CPP(= false);
};

#define wlgt_button1 0x1
#define wlgt_button2 0x2
#define wlgt_button3 0x4

struct wlgt_callbacks {
	void(*error)(wlgt* wnd, void* user, char const* msg) WLGT_IN_CPP(= 0);
	void(*created)(wlgt* wnd, void* user) WLGT_IN_CPP(= 0);
	bool(*closed)(wlgt* wnd, void* user) WLGT_IN_CPP(= 0);
	bool(*destroyed)(wlgt* wnd, void* user) WLGT_IN_CPP(= 0);
	void(*pos_changed)(wlgt* wnd, void* user, int x, int y, int width, int height) WLGT_IN_CPP(= 0);
	void(*size_changed)(wlgt* wnd, void* user, int width, int height) WLGT_IN_CPP(= 0);
	void(*surface_changed)(wlgt* wnd, void* user, int width, int height, unsigned char* bytes) WLGT_IN_CPP(= 0);
	void(*key_down)(wlgt* wnd, void* user, int keyCode, long long time) WLGT_IN_CPP(= 0);
	void(*key_up)(wlgt* wnd, void* user, int keyCode, long long time) WLGT_IN_CPP(= 0);
	void(*button_down)(wlgt* wnd, void* user, int button, int x, int y, float fx, float fy, long long time) WLGT_IN_CPP(= 0);
	void(*button_up)(wlgt* wnd, void* user, int button, int x, int y, float fx, float fy, long long time) WLGT_IN_CPP(= 0);
	void(*cursor_pos)(wlgt* wnd, void* user, int x, int y, float fx, float fy, int buttons, long long time) WLGT_IN_CPP(= 0);
	void(*cursor_delta)(wlgt* wnd, void* user, int x, int y, float fx, float fy, int buttons, long long time) WLGT_IN_CPP(= 0);
};

bool create_thread(void(*run)(void*), void* user, bool(*init)(void*));

wlgt* create_window(char const* name, wlgt_rect const* rect, wlgt_flags const* flags, wlgt_callbacks const* callbacks, void* user, wlgt* owner);
void* window_userdata(wlgt* wnd);

void update_window(wlgt* wnd);
void update_transparent_window(wlgt* wnd);
void show_window(wlgt* wnd, bool show);
void update_window_pos(wlgt* wnd, wlgt_rect const* rect);

bool close_window(wlgt* wnd);
void force_destroy_window(wlgt* wnd);

void async_update_window(wlgt* wnd);
bool join_close_window(wlgt* wnd);
void join_force_destroy_window(wlgt* wnd);
