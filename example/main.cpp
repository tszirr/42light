#define WNDLIGHT_CPP
#include <wndlight.h>
#include <random>
#include <algorithm>

struct layered_ui {
	wlgt* owner;
	wlgt* wnd;

	static bool init(void* ui) {
		layered_ui& self = *(layered_ui*)ui;

		wlgt_rect rect;
		wlgt_callbacks callbacks;

		callbacks.surface_changed = [](wlgt* wnd, void* user, int width, int height, unsigned char* bytes) {
			for (int i = 0; i < height; ++i)
				for (int j = 0; j < width; ++j) {
					int b = 4 * (j + i * width);
					int a = std::max(j * 255 / width - 128, 0);
					bytes[b + 0] = (unsigned char)(rand() % 256 * a / 255);
					bytes[b + 1] = (unsigned char)(rand() % 256 * a / 255);
					bytes[b + 2] = (unsigned char)(rand() % 256 * a / 255);
					bytes[b + 3] = (unsigned char)a;
				}
		};

		wlgt_flags flags;
		flags.transparent = true;
		self.wnd = create_window("Layered UI", &rect, &flags, &callbacks, nullptr, self.owner);

		return true;
	}
	static void run(void* ui) {
		layered_ui& self = *(layered_ui*)ui;
		update_transparent_window(self.wnd);
		wlgt_loop();
	}

	layered_ui() = default;
	void launch(wlgt* owner) {
		this->owner = owner;
		create_thread(run, this, init);
	}
	void join() {
		join_force_destroy_window(wnd);
	}
};

int main() {
	wlgt_init();

	struct Window {
		wlgt* wnd;
		layered_ui layeredUi;

		Window() {
			wlgt_callbacks callbacks;
			callbacks.pos_changed = &posChanged;

			wlgt_rect rect;
			wlgt_flags flags;
			flags.appwindow = flags.decorated = flags.resizeable = flags.bufferless = true;
			wnd = create_window("Hello!", &rect, &flags, &callbacks, this, nullptr);

			layeredUi.launch(wnd);
		}

		void run() {
			show_window(layeredUi.wnd, true);
			show_window(wnd, true);
			wlgt_loop();

			layeredUi.join();
		}

		static void posChanged(wlgt* wnd, void* user, int x, int y, int width, int height) {
			Window& self = *(Window*)user;
			wlgt_rect rect = { x, y, width, height };
			update_window_pos(self.layeredUi.wnd, &rect);
		}

	} mainWindow;
	mainWindow.run();

	wlgt_uninit();

	return 0;
}