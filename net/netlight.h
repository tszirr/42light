#pragma once

#ifdef WNDLIGHT_CPP
 #define NETLIGHT_IN_C(...) 
 #define NETLIGHT_IN_CPP(...) __VA_ARGS__
#else
 #define NETLIGHT_IN_C(...) __VA_ARGS__
 #define NETLIGHT_IN_CPP(...) 
#endif

// throwaway API, no persistent connections? ...
pnlgt* create_port();
bool can_broadcast(pnlgt* port);
bool broadcast(pnlgt* port, char const* msg, int len);
bool can_collect(pnlgt* port);
int collect(pnlgt* port, char* msg, int maxLen, bool wait);
void close_port(pnlgt* port);

cnlgt* create_connection();
bool send(cnlgt* conn, char const* msg, int len);
int receive(cnlgt* conn, char* msg, int maxLen);
void close_connection(cnlgt* cnnt);
